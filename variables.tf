variable "name" {
  description = "Name of the owner"
  type        = string
  default     = "bd_sa_dev"
}

variable "region" {
  description = "The AWS region to deploy to"
  type        =  string
  default     = "us-east-1"
}

variable "environment" {
  description = "The AWS environment to deploy to"
  type        =  string
  default     = "dev"
}

#variable "kms_key_id" {
#  description = "The ARN for the KMS encryption key. If creating an encrypted replica, set this to the destination KMS ARN. If #storage_encrypted is set to true and kms_key_id is not specified the default KMS key created in your account will be used"
#  type        = string
#  default     = "rds"
#}

#variable "performance_insights_kms_key_id" {
#  description = "The ARN for the KMS key to encrypt Performance Insights data."
#  type        = string
#  default     = "rds"
#}

variable "deletion_protection" {
  description = "The database can't be deleted when this value is set to true."
  type        = bool
  default     = true
}

variable "performance_insights_enabled" {
  description = "Specifies whether Performance Insights are enabled"
  type        = bool
  default     = true
}

variable "vpc_id" {
  type        = string
  description = "ID of an existing VPC in which to create the cluster."
  default     = "vpc-0824255a4802d8004"
}

variable "vpc_security_group_ids" {
  description = "List of VPC security groups to associate"
  type        = list(string)
  default     = ["sg-0e1ccc4c05b4d380b","sg-091f2010dd9ad070f"]
}

variable "subnet_ids" {
  description = "A list of VPC subnet IDs"
  type        = list(string)
  default     = ["172.17.6.0/24"]
}
/*variable "db_subnet_group_name" {
  description = "Name of DB subnet group. DB instance will be created in the VPC associated with the DB subnet group. If unspecified, will be created in the default VPC"
  type        = string
  default     = null
}
db_subnet_group_name   = local.db_subnet_group_name
/*
variable "create_db_subnet_group" {
  description = "Whether to create a database subnet group"
  type        = bool
  default     = true
}
*/
