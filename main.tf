provider  "aws"{
 region = local.region
}

locals {
  name   = "dev-broadrige-lrp-db-test"
  region = "us-east-1"
  tags = {
    Project       : "Legal-RM",
    Environment : "DEV"
  }
}

################################################################################
# Supporting Resources
################################################################################
/*
data "aws_vpc" "selected" {
  id = var.vpc_id
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.selected.id
}

resource "aws_db_subnet_group" "example" {
  name       = local.name
  subnet_ids = var.subnet_ids


    tags = local.tags

}
*/

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 2"

  name = "vpc_${local.name}"
  cidr = "172.17.0.0/18"

  azs              = ["${local.region}a", "${local.region}b", "${local.region}c"]
  public_subnets   = ["172.17.0.0/24", "172.17.1.0/24", "172.17.2.0/24"]
  private_subnets  = ["172.17.3.0/24", "172.17.4.0/24", "172.17.5.0/24"]
  database_subnets = ["172.17.7.0/24", "172.17.8.0/24", "172.17.9.0/24"]

  create_database_subnet_group = true

  tags = local.tags
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4"

  name        = "sg_${local.name}"
  description = "Complete SqlServer example security group"
  vpc_id      = module.vpc.vpc_id

  # ingress
  ingress_with_cidr_blocks = [
    {
      from_port   = 1433
      to_port     = 1433
      protocol    = "tcp"
      description = "SqlServer access from within VPC"
      cidr_blocks = module.vpc.vpc_cidr_block
      #cidr_blocks       = ["0.0.0.0/0"]
    },
  ]

  tags = local.tags
}


################################################################################
# IAM Role for Windows Authentication
################################################################################

data "aws_iam_policy_document" "example" {
  
    version = "2012-10-17"
    statement  { 
            sid = "VisualEditor0"
            effect = "Allow"
            actions = [
                "s3:PutObject",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:GetBucketAcl",
                "s3:GetBucketLocation",
            ]
            resources = [
                "arn:aws:s3:::brdocumentsupload",
                "arn:aws:s3:::brdocumentsupload/*",
            ]
        }
    statement  {
            sid = "VisualEditor1"
            effect = "Allow"
            actions = ["s3:ListAllMyBuckets",]
            resources = ["*",]
        }
    


}

resource "aws_iam_policy" "example" {
  name   = "policy_s3_${local.name}"
  path   = "/"
  policy = data.aws_iam_policy_document.example.json
}


resource "aws_iam_role" "example" {
  name                  = "role_${local.name}"
  description           = "Role used by RDS for accesing S3 objects"
  #force_detach_policies = true
  assume_role_policy    = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "rds.amazonaws.com"
        }
      },
    ]
  })
  tags = local.tags
}

resource "aws_iam_role_policy_attachment" "rds_directory_services" {
  role       = aws_iam_role.example.id
  policy_arn = aws_iam_policy.example.arn
}

data "aws_kms_alias" "rds" {
  name = "alias/aws/rds"
}

resource "aws_db_parameter_group" "rds_parameter_group" {
  name   = "dev-dbparameter-${local.name}"
  family = "sqlserver-web-15.0"

  parameter {
	apply_method = "pending-reboot"
    name  = "rds.force_ssl"
    value = 1
  }

}

resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

################################################################################
# RDS Module
################################################################################

module "rds" {
  source				    = "terraform-aws-modules/rds/aws"
  identifier			    = local.name
  engine               	= "sqlserver-web"
  engine_version       	= "15.00.4073.23.v1"
  family               	= "sqlserver-web-15.0" # DB parameter group
  major_engine_version 	= "15.00"             # DB option group
  instance_class       	= "db.m5.large"       # chnaged instance from large to small
  kms_key_id        = data.aws_kms_alias.rds.target_key_arn
  performance_insights_enabled          = var.performance_insights_enabled
  performance_insights_kms_key_id       = data.aws_kms_alias.rds.target_key_arn

  allocated_storage     = 20
  max_allocated_storage = 25                 # max 100 to 25
  storage_encrypted     = true

  name                   = null
  username               = "admin"    # changed user name
  password               = random_password.password.result
  create_random_password = false
  
  random_password_length = 12          # chnaged length from 12 to 8
  port                   = 1433

  #domain               = aws_directory_service_directory.demo.id
  domain_iam_role_name = aws_iam_role.example.name

  multi_az               = false
  #subnet_ids             = module.vpc.database_subnets
  subnet_ids             = slice(tolist(module.vpc.database_subnets), 0, 2)
  #subnet_ids              = aws_db_subnet_group.example.id
  vpc_security_group_ids = [module.security_group.security_group_id]

  maintenance_window              = "Mon:00:00-Mon:03:00"
  backup_window                   = "03:00-06:00"
  enabled_cloudwatch_logs_exports = ["error","agent"]

  backup_retention_period = 15
  skip_final_snapshot     = true
  deletion_protection      = var.deletion_protection

  performance_insights_retention_period = 7
  create_monitoring_role                = true
  monitoring_interval                   = 60

  options                   = []
  create_db_parameter_group = false
  parameter_group_name   = aws_db_parameter_group.rds_parameter_group.id
  license_model             = "license-included"
  timezone                  = "GMT Standard Time"
  character_set_name        = "Latin1_General_CI_AS"

  tags = local.tags
}

resource "aws_db_instance_role_association" "example" {
  db_instance_identifier = local.name
  feature_name           = "S3_INTEGRATION"
  role_arn               = aws_iam_role.example.arn

  depends_on = [module.rds, aws_iam_role.example]
}

resource "aws_secretsmanager_secret" "rds_credentials" {
  name = "${local.name}_credentials"

  depends_on = [module.rds]

  tags = local.tags
}

resource "aws_secretsmanager_secret_version" "rds_credentials" {
  secret_id     = aws_secretsmanager_secret.rds_credentials.id
  secret_string = <<EOF
  {
  "username": "${module.rds.db_instance_username}",
  "password": ${random_password.password.result},
  "engine": "sqlserver-web",
  "host": "${module.rds.db_instance_endpoint}",
  "port": ${module.rds.db_instance_port},
  "identifier": ${local.name}
  }
  EOF

  depends_on = [module.rds]

}

